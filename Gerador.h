/* 
 * File:   Gerador.h
 * Author: Emanuel Marques
 *
 * Created on 23 de Outubro de 2014, 16:53
 */

#ifndef GERADOR_H
#define	GERADOR_H
#include <cstdlib>
#include <stdlib.h>
#include <stack>
#include <ctime>
#include <ostream>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "Armazem.h"
#include "Deposito.h"
#include "DepositoFresco.h"
#include "DepositoNormal.h"

using namespace std;

class Gerador {
private:
    int random(int min, int max);
    void exportarMatriz(string file, const Armazem& armazem) const;

public:
    Gerador();
    ~Gerador();
    Armazem gera(int min, int max);

    void splitString(const string &s, char delim, vector<string> &elems);
    void gerarGrafo(string file, Armazem& armazem);

};

Gerador::Gerador() {

}

Gerador::~Gerador() {

}

int Gerador::random(int min, int max) {

    return (min + (rand() % (int) (max - min + 1)));
}

Armazem Gerador::gera(int min, int max) {

    srand(time(NULL));

    int numDepositosNormais = random(min, max);
    int numDepositosFrescos = random(min, max);
    int qtdPaletesNormais;
    int qtdPaletesFrescas;
    int capMaxPaletesNormais;
    int capMaxPaletesFrescas;
    int area;
    Armazem a;

    //    cria os depositos
    for (int i = 0; i < numDepositosNormais; i++) {
        capMaxPaletesNormais = random(min, max);
        qtdPaletesNormais = random(min, max);
        area = random(min, max);
        a.novoDepositoNormal(capMaxPaletesNormais, area, qtdPaletesNormais);

    }
    for (int i = 0; i < numDepositosFrescos; i++) {
        capMaxPaletesFrescas = random(min, max);
        qtdPaletesFrescas = random(min, max);
        area = random(min, max);
        a.novoDepositoFresco(capMaxPaletesFrescas, area, qtdPaletesFrescas);
    }

    //    adiciona as paletes nos depositos
    typename map<int, Deposito* >::iterator itr;
    map<int, Deposito* > mapRegisto = a.getRegisto();

    for (itr = mapRegisto.begin(); itr != mapRegisto.end(); ++itr) {
        int qntPaletes = (*itr).second->getNrPaletes();
        for (int i = 0; i <= qntPaletes; i++) {
            (*itr).second->addPalete();

        }
    }
    a.setRegisto(mapRegisto);

    //    gera as distancias entre depositos aleatoriamente
    vector<vector<int> > matrixDistancias;
    vector<vector<int> >::iterator itrV;

    int totalDepositos = numDepositosFrescos + numDepositosNormais;
    for (int i = 0; i < totalDepositos; i++) {
        vector<int> temp;
        matrixDistancias.push_back(temp);
    }

    // preencher matriz a zeros
    for (itrV = matrixDistancias.begin(); itrV != matrixDistancias.end(); ++itrV) {
        for (int k = 0; k < totalDepositos; k++) {
            (*itrV).push_back(0);
        }
    }

    // preenche meia matriz
    int c = 0;
    for (int j = 0; j < totalDepositos; j++) {
        for (int i = c; i < totalDepositos; i++) {
            matrixDistancias[j][i] = random(min + 1, max);
        }
        c++;
    }

    // reflexao da matriz
    c = totalDepositos - 1;
    for (int j = totalDepositos - 1; j >= 0; j--) {
        for (int i = 0; i < c; i++) {
            matrixDistancias[j][i] = random(min + 1, max);
        }
        c--;
    }

    a.setDistancias(matrixDistancias);
    this->exportarMatriz("matriz.txt", a);
    return a;
}

void Gerador::exportarMatriz(string file, const Armazem& armazem) const {

    ofstream outfile;
    outfile.open(file);
    if (outfile.is_open()) {
        vector<vector<int> > temp = armazem.getDistancias();
        int i = 0;
        while (!temp.empty()) {
            vector<int> tempVec = temp.front();
            int j = 0;
            while (!tempVec.empty()) {
                // keyDeposito;keyDeposito2;distancia
                outfile << i << ";" << j << ";" << tempVec.front() << endl;
                j++;
                tempVec.pop_back();
            }
            i++;
            temp.pop_back();
        }
        outfile.close();
    } else {
        cout << "Impossível escrever para esse ficheiro!" << endl;
    }

}

void Gerador::splitString(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        if (!item.empty()) elems.push_back(item);
    }
}

void Gerador::gerarGrafo(string file, Armazem& armazem) {

    ifstream inputFile;
    inputFile.open(file);

    if (inputFile.is_open()) {
        while (inputFile.eof()) {
            string temp;
            getline(inputFile, temp, '\n');

            vector<string> parts;
            this->splitString(temp, ';', parts);


            Deposito * d1;
            Deposito * d2;
            int d1_key;
            int d2_keY;
            map<int, Deposito*>::iterator itr;
            for (itr = armazem.getRegisto().begin(); itr != armazem.getRegisto().end(); ++itr) {
                string d1K = parts.front();
                d1_key = atoi(d1K.c_str());
                if (d1_key == (*itr).first) {
                    d1 = (*itr).second;
                }
                itr = armazem.getRegisto().end();
            }

            for (itr = armazem.getRegisto().begin(); itr != armazem.getRegisto().end(); ++itr) {
                string d2K = parts[1];
                d2_keY = atoi(d2K.c_str());
                if (d2_keY == (*itr).first) {
                    d2 = (*itr).second;
                }
                itr = armazem.getRegisto().end();
            }

            vector<vector<int> > tempDist = armazem.getDistancias();
            vector<int> tempDistC = tempDist[d1_key];
            int dist = tempDistC[d2_keY];

            armazem.addGraphEdge(dist, d1, d2);


        }
    }
}




#endif	/* GERADOR_H */

