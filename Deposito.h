/* 
 * File:   Depósito.h
 * Author: Emanuel Marques
 *
 * Created on 2 de Outubro de 2014, 17:08
 */

#ifndef DEPOSITO_H
#define	DEPOSITO_H

using namespace std;

#include <vector>
#include <ostream>

class Deposito {
private:
    int capMaxPaletes;
    float area;
    int nrPaletes;


public:
    Deposito();
    Deposito(int maxP, float area, int nrPaletes);
    Deposito(const Deposito &d);
    virtual ~Deposito();
    virtual Deposito* clone() const = 0;

    virtual void addPalete() = 0;
    virtual bool armazenarProdutos(const vector<string> produtos) = 0;
    virtual vector<string> retirarProdutos(int qntProdutos) = 0;
    virtual bool podeAdicionar() const = 0;
    virtual bool podeRemover() const = 0;
    virtual void escrever(ostream& out) const;

    bool operator==(const Deposito& p)const;
    bool operator!=(const Deposito& p)const;

    void setArea(float area);
    float getArea() const;
    void setCapMaxPaletes(int capMaxPaletes);
    int getCapMaxPaletes() const;
    void setKeyPalete(int keyPalete);
    int getKeyPalete() const;
    void setNrPaletes(int nrPaletes);
    int getNrPaletes() const;

};

bool Deposito::operator==(const Deposito& p) const {

    if ((this->capMaxPaletes == p.capMaxPaletes && this->area == p.area)) {
        return true;

    }
    return false;
}

bool Deposito::operator!=(const Deposito& p) const {

    if ((this->capMaxPaletes == p.capMaxPaletes && this->area == p.area)) {
        return false;

    }
    return true;
}

Deposito::Deposito() {

    this->capMaxPaletes = 0;
    this->area = 0;

}

Deposito::Deposito(int maxP, float area, int nrPaletes) {

    this->capMaxPaletes = maxP;
    this->area = area;
    this->nrPaletes = nrPaletes;
}

Deposito::Deposito(const Deposito &d) {

    this->capMaxPaletes = d.capMaxPaletes;
    this->area = d.area;
    this->nrPaletes = d.nrPaletes;


}

Deposito::~Deposito() {

}

void Deposito::setArea(float area) {
    this->area = area;
}

float Deposito::getArea() const {
    return area;
}

void Deposito::setCapMaxPaletes(int capMaxPaletes) {
    this->capMaxPaletes = capMaxPaletes;
}

int Deposito::getCapMaxPaletes() const {
    return capMaxPaletes;
}

void Deposito::setNrPaletes(int nrPaletes) {
    this->nrPaletes = nrPaletes;
}

int Deposito::getNrPaletes() const {
    return nrPaletes;
}

void Deposito::escrever(ostream& out) const {
    out << "Numero de paletes: " << nrPaletes << endl;
    out << "Capacidade maxima das paletes: " << capMaxPaletes << endl;
    out << "Area ocupada: " << area << endl;
}

ostream& operator<<(ostream& out, const Deposito& dep) {
    dep.escrever(out);
    return out;
}

#endif	/* DEPÓSITO_H */

