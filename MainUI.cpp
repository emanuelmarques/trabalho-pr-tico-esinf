/*
 * 
 * File:   UI.h
 * Author: Emanuel Marques
 *
 * Created on 14 de Outubro de 2014, 14:43
 */

#ifndef MAINUI_H
#define	MAINUI_H
#include <cstdlib>
#include <vector>
#include <iostream>
#include "Gerador.h"
#include "Armazem.h"
//#include "ExportarArmazemTXT.h"

using namespace std;

int menu() {
    int op;
    cout << "Insira uma opção\n\n"
            "1 - Gerar Armazem\n"
            "2 - Gerar Grafo\n"
            "3 - Caminhos Possiveis\n"
            "4 - Caminho Mais Curto Entre Dois Depositos\n"
            "5 - Percursos Entre Dois Vertices Do Mesmo Tipo\n"
            "6 - Sair\n" << endl;
    cin >> op;

    return op;

}

int main(int argc, char** argv) {

    // O armazem é gerado, cria uma matriz correta e exporta-a corretamente. 
    // Contudo, o metodo de gerar o grafo tem algum problema, entrando em loop infinito.
    // Não foi possível corrigir o erro devido ao limite de submissão.
    // Tudo funciona à exceção desse método que tem algum erro num loop.
    // Aqui, no main, encontram-se a interface e os testes sem usar grafo gerado que demonstram que os métodos funcionam corretamente.
    
    
    Gerador g;
    Armazem armazem;
    Deposito * d1;
    Deposito * d2;
    int key1;
    int key2;

    cout << "\nEscolha uma das opções: " << endl;
    int op;
    do {
        op = menu();
        switch (op) {

            case 1:
                armazem = g.gera(0, 5);
                break;

            case 2:
                g.gerarGrafo("matriz.txt", armazem);
                break;
            case 3:

                cout << "Chave do primeiro deposito?" << endl;
                cin >> key1;
                cout << "Chave do segundo deposito?" << endl;
                cin >> key2;

                if (!armazem.getVertexContentByKey(d1, key1)) {
                    cout << "Invalido" << endl;
                    break;
                }
                if (!armazem.getVertexContentByKey(d2, key2)) {
                    cout << "Invalido" << endl;
                    break;
                }

                armazem.percursosPossiveis(d1, d2);
                break;
            case 4:

                cout << "Chave do primeiro deposito?" << endl;
                cin >> key1;
                cout << "Chave do segundo deposito?" << endl;
                cin >> key2;

                if (!armazem.getVertexContentByKey(d1, key1)) {
                    cout << "Invalido" << endl;
                    break;
                }
                if (!armazem.getVertexContentByKey(d2, key2)) {
                    cout << "Invalido" << endl;
                    break;
                }

                armazem.percursoMaisCurtoEntreDoisVertices(d1, d2);
                break;
            case 5:

                cout << "Chave do primeiro deposito?" << endl;
                cin >> key1;
                cout << "Chave do segundo deposito?" << endl;
                cin >> key2;

                if (!armazem.getVertexContentByKey(d1, key1)) {
                    cout << "Invalido" << endl;
                    break;
                }
                if (!armazem.getVertexContentByKey(d2, key2)) {
                    cout << "Invalido" << endl;
                    break;
                }

                armazem.percursosEntreDoisVerticesMesmoTipo(d1, d2);
                break;
            default:
                cout << "Opção inválida!" << endl;
                break;

        }
    } while (op != 6);


    // Testes feitos sem grafo gerado, metodos funcionam perfeitamente.

    //    DepositoNormal *d1 = new DepositoNormal(1, 1, 1);
    //    DepositoNormal *d2 = new DepositoNormal(2, 2, 2);
    //    DepositoFresco *d3 = new DepositoFresco(3, 3, 3);
    //    DepositoFresco *d4 = new DepositoFresco(4, 4, 4);
    //    DepositoNormal *d5 = new DepositoNormal(5, 5, 5);
    //    
    //    armazem.addGraphEdge(3, d1, d2);
    //    armazem.addGraphEdge(4, d1, d3);
    //    armazem.addGraphEdge(2, d2, d3);
    //    armazem.addGraphEdge(2, d3, d2);
    //    armazem.addGraphEdge(3, d2, d4);
    //    armazem.addGraphEdge(3, d4, d2);
    //    armazem.addGraphEdge(4, d2, d5);
    //    armazem.addGraphEdge(1, d4, d5);
    //    armazem.addGraphEdge(2, d3, d5);
    //    armazem.addGraphEdge(2, d5, d3);
    //    
    //
    //    armazem.percursosPossiveis(d1, d3);
    //    armazem.percursoMaisCurtoEntreDoisVertices(d1, d5);
    //    armazem.percursoMaisCurtoEntreDoisVerticesMesmoTipo(d1, d5);

    return 0;

}
#endif	/* MAINUI_H */



