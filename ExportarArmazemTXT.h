/* 
 * File:   ExportarArmazemTXT.h
 * Author: vitor_000
 *
 * Created on 24 de Outubro de 2014, 16:34
 */

#ifndef EXPORTARARMAZEMTXT_H
#define	EXPORTARARMAZEMTXT_H

using namespace std;

#include <ostream>
#include <fstream>
#include <iostream>
#include "ExportarArmazem.h"

class ExportarArmazemTXT : public ExportarArmazem {
private:
    
public:
    ExportarArmazemTXT();
    ~ExportarArmazemTXT();
    
    void exportar(string file, const Armazem& armazem) const;
    
};

ExportarArmazemTXT::ExportarArmazemTXT() {
    
}

ExportarArmazemTXT::~ExportarArmazemTXT() {
    
}

void ExportarArmazemTXT::exportar(string file, const Armazem& armazem) const {
    ofstream outfile;
    outfile.open(file);
    if (outfile.is_open()) {
        outfile << armazem;
        outfile.close();
    } else {
        cout << "Impossível escrever para esse ficheiro!" << endl;
    }
    
}

#endif	/* EXPORTARARMAZEMTXT_H */

