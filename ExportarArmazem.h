/* 
 * File:   ExportarArmazem.h
 * Author: vitor_000
 *
 * Created on 24 de Outubro de 2014, 16:34
 */

#ifndef EXPORTARARMAZEM_H
#define	EXPORTARARMAZEM_H
#include <string>
#include "Armazem.h"
using namespace std;

class ExportarArmazem
{
public:
	ExportarArmazem();
	virtual ~ExportarArmazem();
	virtual void exportar(string file, const Armazem& armazem) const = 0;

private:

};

ExportarArmazem::ExportarArmazem()
{
}

ExportarArmazem::~ExportarArmazem()
{
}

#endif	/* EXPORTARARMAZEM_H */

