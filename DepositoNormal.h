/* 
 * File:   DepositoNormal.h
 * Author: Vitor Moreira
 *
 * Created on 13 de Outubro de 2014, 22:29
 */

#ifndef DEPOSITONORMAL_H
#define	DEPOSITONORMAL_H

using namespace std;

#include <vector>
#include <stack>
#include "Deposito.h"
#include <ostream>

class DepositoNormal : public Deposito {
private:
    vector<stack<string> > paletes;
    int numPaleteAdicionar;
    int numPaleteRemover;
    int totalProdutos;
    int qntProdutosAtual;

public:
    DepositoNormal();
    DepositoNormal(int capMaxPaletes, float area, int nrPaletes);
    DepositoNormal(const DepositoNormal& dn);
    ~DepositoNormal();
    virtual DepositoNormal* clone() const;

    void setPaletes(vector<stack<string> > paletes);
    vector<stack<string> > getPaletes() const;

    bool armazenarProdutos(const vector<string> produtos);
    vector<string> retirarProdutos(int qntProdutos);
    bool podeAdicionar() const;
    bool podeRemover() const;
    void addPalete();

    void escrever(ostream& out) const;
};

DepositoNormal* DepositoNormal::clone() const {
    return new DepositoNormal(*this);
}

DepositoNormal::DepositoNormal() : Deposito(), paletes() {
    numPaleteAdicionar = 0;
    numPaleteRemover = 0;
    totalProdutos = 0;
    qntProdutosAtual = 0;

}

DepositoNormal::DepositoNormal(int maxPaletes, float area, int nrPaletes) : Deposito(maxPaletes, area, nrPaletes), paletes() {
    numPaleteAdicionar = 0;
    numPaleteRemover = 0;
    if (nrPaletes % 2 == 0) {
        totalProdutos = (maxPaletes * (nrPaletes / 2) + (maxPaletes / 2)*(nrPaletes / 2));
    } else {
        totalProdutos = (maxPaletes * (nrPaletes * (2 / 3)) + (maxPaletes / 2)*(nrPaletes * (1 / 3)));
    }
    qntProdutosAtual = 0;
}

DepositoNormal::DepositoNormal(const DepositoNormal& dn) : Deposito(dn), paletes(dn.paletes) {

}

DepositoNormal::~DepositoNormal() {

}

void DepositoNormal::setPaletes(vector<stack<string> > palete) {
    paletes = palete;
}

vector<stack<string> > DepositoNormal::getPaletes() const {
    return paletes;
}

void DepositoNormal::addPalete() {
    stack<string> a;
    this->paletes.push_back(a);
}

bool DepositoNormal::podeAdicionar() const {
    return (this->qntProdutosAtual < this->totalProdutos);
}

bool DepositoNormal::podeRemover() const {
    return (this->qntProdutosAtual > 0);
}

bool DepositoNormal::armazenarProdutos(const vector<string> produtos) {

    if (!this->podeAdicionar()) {
        return false;
    }

    int cont = 0;
    // empilha as pares
    for (int i = 0; i < this->paletes.size(); i += 2) {
        for (int j = 0; j < this->getCapMaxPaletes(); j++) {
            if (cont == produtos.size()) {
                return true;
            }
            this->paletes[i].push(produtos[j]);
            cont++;
        }

    }

    // empilha as impares
    for (int i = 1; i < this->paletes.size(); i+=2) {
        for (int j = 0; j < (this->getCapMaxPaletes() / 2); j++) {
            if (cont == produtos.size()) {
                return true;
            }
            this->paletes[i].push(produtos[j]);
            cont++;
        }

    }

    if (cont < produtos.size() - 1) {
        cout << "Não foram empilhados todos os produtos. Paletes cheias!" << endl;
        return false;
    }
    return true;

}

vector<string> DepositoNormal::retirarProdutos(int qntProdutos) {
    
    vector<string> temp;
    int cont = 0;
    // desempilha as impares
    for (int i = 1; i < this->paletes.size(); i += 2) {

        for (int j = 0; j < (this->getCapMaxPaletes() / 2); j++) {
            if (cont == qntProdutos) {
                return temp;
            }
            temp.push_back(paletes[i].top());
            this->paletes[i].pop();
            cont++;
        }
    }

    // desempilha as pares
    for (int i = 0; i < this->paletes.size(); i += 2) {
        for (int j = 0; j < this->getCapMaxPaletes(); j++) {
            if (cont == qntProdutos) {
                return temp;
            }
            temp.push_back(paletes[i].top());
            this->paletes[i].pop();
            cont++;
        }
    }
    if (cont < qntProdutos - 1) {
        cout << "Não foram desempilhados todos os produtos pedidos. Quantidade pedida superior à existente!" << endl;
    }
    return temp;
}

void DepositoNormal::escrever(ostream& out) const {
    out << "Tipo: Deposito Normal" << endl;
    Deposito::escrever(out);
    stack<string> temp;
    typename vector<stack<string> >::const_iterator itr;
    for (itr = paletes.begin(); itr != paletes.end(); ++itr) {
        temp = *itr;
        while (!temp.empty()) {
            cout << "Palete: " << temp.top() << endl;
            temp.pop();
        }
    }
    out << endl;
}

ostream& operator<<(ostream& out, DepositoNormal& dn) {
    dn.escrever(out);
    return out;
}

#endif	/* DEPOSITONORMAL_H */

