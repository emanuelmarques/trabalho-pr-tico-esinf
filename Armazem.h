/* 
 * File:   Armazem.h
 * Author: Emanuel Marques
 *
 * Created on 2 de Outubro de 2014, 17:06
 */

#ifndef ARMAZEM_H
#define	ARMAZEM_H

#include "Deposito.h"
#include "DepositoFresco.h"
#include "DepositoNormal.h"
#include "graphStlPath.h"
#include <map>
#include <ostream>
#include <iostream>


using namespace std;

class Armazem : public graphStlPath<Deposito*, int> {
private:
    vector<vector<int> > distancias;
    map<int, Deposito* > registo;
    int chaveAtual;
    int numDepositosNormais;
    int numDepositosFrescos;
    int aumentaChave();
    void ordenaCaminho(stack<Deposito*>& caminho) const;
    void imprimirPercursos(queue<stack<Deposito*> >& percursos);
    void imprimePercurso(stack<Deposito*>& percurso);
    void menorCaminho(vector <int> pathkeys, int keyVfinal);

public:
    Armazem();
    Armazem(const Armazem& copia);
    ~Armazem();
    virtual Armazem* clone() const;

    void setChaveAtual(int chaveAtual);
    int getChaveAtual() const;
    map<int, Deposito* > getRegisto() const;
    void setRegisto(map<int, Deposito*> registo);
    void setDistancias(vector<vector<int> > distancias);
    vector<vector<int> > getDistancias() const;

    void novoDepositoNormal(int maxP, int area, int nrPaletes);
    void novoDepositoFresco(int maxP, int area, int nrPaletes);

    void percursosPossiveis(Deposito* d1, Deposito* d2);
    void percursoMaisCurtoEntreDoisVertices(Deposito* d1, Deposito* d2);
    void percursosEntreDoisVerticesMesmoTipo(Deposito* d1, Deposito* d2);

    void escrever(ostream& out) const;

};

Armazem* Armazem::clone() const {
    return new Armazem(*this);
}

Armazem::Armazem() : distancias(), registo() {
    this->chaveAtual = 0;
    this->numDepositosFrescos = 0;
    this->numDepositosNormais = 0;
}

Armazem::Armazem(const Armazem& copia) : distancias(copia.distancias), registo(copia.registo) {
    this->chaveAtual = copia.chaveAtual;

}

Armazem::~Armazem() {
}

void Armazem::setChaveAtual(int chaveAtual) {
    this->chaveAtual = chaveAtual;
}

int Armazem::getChaveAtual() const {
    return chaveAtual;
}

map<int, Deposito* > Armazem::getRegisto() const {
    return registo;
}

void Armazem::setRegisto(map<int, Deposito*> registo) {
    this->registo = registo;
}

void Armazem::setDistancias(vector<vector<int> > distancias) {
    this->distancias = distancias;
}

vector<vector<int> > Armazem::getDistancias() const {
    return distancias;
}

int Armazem::aumentaChave() {
    this->chaveAtual++;
    return this->chaveAtual;
}

void Armazem::ordenaCaminho(stack<Deposito*>& caminho) const {
    stack<Deposito*> temp = caminho;
    while (!caminho.empty()) {
        caminho.pop();
    }
    while (!temp.empty()) {
        caminho.push(temp.top());
        temp.pop();
    }
}

void Armazem::imprimirPercursos(queue<stack<Deposito*> >& percursos) {
    while (!percursos.empty()) {
        stack<Deposito*> temp = percursos.front();
        imprimePercurso(temp);
        percursos.pop();
    }
}

void Armazem::imprimePercurso(stack<Deposito*>& percurso) {
    while (!percurso.empty()) {
        int key;
        this->getVertexKeyByContent(key, percurso.top());
        cout << key;
        percurso.pop();
        if (!percurso.empty()) {
            cout << " -> ";
        }
    }
    cout << endl;
    cout << "--------------------" << endl;
}

void Armazem::novoDepositoNormal(int maxP, int area, int nrPaletes) {
    DepositoNormal d(maxP, area, nrPaletes);
    this->numDepositosNormais++;
    registo[aumentaChave()] = d.clone();

}

void Armazem::novoDepositoFresco(int maxP, int area, int nrPaletes) {
    DepositoFresco d(maxP, area, nrPaletes);
    this->numDepositosFrescos++;
    registo[aumentaChave()] = d.clone();

}

void Armazem::percursosPossiveis(Deposito* d1, Deposito* d2) {

    int keyD1;
    int keyD2;
    if (!this->getVertexKeyByContent(keyD1, d1)) {
        cout << "\nDeposito 1 inválido, ou não existente!" << endl;
        return;
    }
    if (!this->getVertexKeyByContent(keyD2, d2)) {
        cout << "\nDeposito 2 inválido, ou não existente!" << endl;
        return;
    }


    queue<stack<Deposito*> > percursosAux = this->distinctPaths(d1, d2);
    queue<stack<Deposito*> > percursos;

    cout << "\nCaminhos possiveis: " << endl;
    cout << "Origem:  Deposito " << keyD1 << endl;
    cout << "Destino: Deposito " << keyD2 << endl;
    while (!percursosAux.empty()) {
        stack<Deposito*> temp = percursosAux.front();
        ordenaCaminho(temp);
        percursos.push(temp);
        percursosAux.pop();
    }

    imprimirPercursos(percursos);
}

void Armazem::percursoMaisCurtoEntreDoisVertices(Deposito* d1, Deposito* d2) {

    list< graphVertex<Deposito*, int> >::iterator itr;
    if (!this->getVertexIteratorByContent(itr, d1)) {
        cout << "\nDeposito 1 inválido, ou não existente!" << endl;
        return;
    }
    if (!this->getVertexIteratorByContent(itr, d2)) {
        cout << "\nDeposito 2 inválido, ou não existente!" << endl;
        return;
    }

    int keyD1;
    int keyD2;
    vector<int> pathKeys;
    vector<int> dist;
    this->getVertexKeyByContent(keyD1, d1);
    this->getVertexKeyByContent(keyD2, d2);
    this->dijkstrasAlgorithm(d1, pathKeys, dist);

    cout << "\nCaminho mais curto: " << endl;
    cout << "Custo do caminho mais curto = " << dist[keyD2] << endl;
    cout << "Origem:  Deposito " << keyD1 << endl;
    cout << "Destino: Deposito " << keyD2 << endl;
    this->menorCaminho(pathKeys, keyD2);
}

void Armazem::percursosEntreDoisVerticesMesmoTipo(Deposito* d1, Deposito* d2) {

    queue<stack<Deposito*> > percursos = this->distinctPaths(d1, d2);

    // auxiliares
    bool flag;
    stack<Deposito*> stackAux;
    Deposito * temp;
    stack<Deposito*> percurso;

    while (!percursos.empty()) {

        flag = true;
        stackAux = percursos.front();
        while (!stackAux.empty()) {
            temp = stackAux.top();

            //comparar tipos de deposito
            if (flag && typeid (*d1) != typeid (*temp)) {
                flag = false;
            }
            percurso.push(stackAux.top());
            stackAux.pop();
        }

        //se todos os depósitos sao do mesmo tipo, imprime o percurso
        if (flag) {
            int keyD1;
            int keyD2;
            this->getVertexKeyByContent(keyD1, d1);
            this->getVertexKeyByContent(keyD2, d2);
            cout << endl;
            cout << "\nPercurso entre os depositos do mesmo tipo: " << endl;
            cout << "Origem:  Deposito " << keyD1 << endl;
            cout << "Destino: Deposito " << keyD2 << endl;
            imprimePercurso(percurso);
            return;
        } else {
            // limpar percurso pois nao sao todos do mesmo tipo
            while (!percurso.empty()) {
                percurso.pop();
            }
        }
        percursos.pop();
    }
}

void Armazem::menorCaminho(vector <int> pathkeys, int keyVfinal) {

    if (keyVfinal != -1) {
        int key = pathkeys[keyVfinal];
        menorCaminho(pathkeys, key);
        cout << keyVfinal << " -> ";

    }
}

void Armazem::escrever(ostream & out) const {
    typename map<int, Deposito* >::const_iterator itr;
    for (itr = this->registo.begin(); itr != this->registo.end(); ++itr) {
        cout << "Chave: " << (*itr).first << endl;
        (*itr).second->escrever(out);
    }
    out << "---------------------" << endl;
}

ostream& operator<<(ostream& out, Armazem & armazem) {
    armazem.escrever(out);
    return out;
}

#endif	/* ARMAZEM_H */

