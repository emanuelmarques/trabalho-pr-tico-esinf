/* 
 * File:   DepositoFresco.h
 * Author: Vitor Moreira
 *
 * Created on 13 de Outubro de 2014, 22:28
 */

#ifndef DEPOSITOFRESCO_H
#define	DEPOSITOFRESCO_H

using namespace std;

#include <vector>
#include <queue>
#include <string>
#include "Deposito.h"

class DepositoFresco : public Deposito {
private:
    vector<queue<string> > paletes;
    int numPaleteAdicionar;
    int numPaleteRemover;
    int totalProdutos;
    int qntProdutosAtual;

public:
    DepositoFresco();
    DepositoFresco(int capMaxPaletes, float area, int nrPaletes);
    DepositoFresco(const DepositoFresco& df);
    ~DepositoFresco();
    virtual DepositoFresco* clone() const;

    void setPaletes(vector<queue<string> > paletes);
    vector<queue<string> > getPaletes() const;

    bool armazenarProdutos(const vector<string> produtos);
    vector<string> retirarProdutos(int qntProdutos);
    void addPalete();
    bool podeAdicionar() const;
    bool podeRemover() const;
    void escrever(ostream& out) const;

};

DepositoFresco* DepositoFresco::clone() const {
    return new DepositoFresco(*this);
}

DepositoFresco::DepositoFresco() : Deposito(), paletes() {
    numPaleteAdicionar = 0;
    numPaleteRemover = 0;
    totalProdutos = 0;
    qntProdutosAtual = 0;
}

DepositoFresco::DepositoFresco(int maxPaletes, float area, int nrPaletes) : Deposito(maxPaletes, area, nrPaletes), paletes() {
    numPaleteAdicionar = 0;
    numPaleteRemover = 0;
    qntProdutosAtual = 0;
    totalProdutos = nrPaletes * maxPaletes;
}

DepositoFresco::DepositoFresco(const DepositoFresco& df) : Deposito(df), paletes(df.paletes) {
    numPaleteAdicionar = df.numPaleteAdicionar;
    numPaleteRemover = df.numPaleteRemover;
    totalProdutos = df.totalProdutos;
    qntProdutosAtual = df.qntProdutosAtual;

}

DepositoFresco::~DepositoFresco() {

}

void DepositoFresco::setPaletes(vector<queue<string> > palete) {
    this->paletes = palete;
}

vector<queue<string> > DepositoFresco::getPaletes() const {
    return this->paletes;
}

void DepositoFresco::addPalete() {
    queue<string> a;
    this->paletes.push_back(a);
}

bool DepositoFresco::podeAdicionar() const {
    return (this->qntProdutosAtual < this->totalProdutos);
}

bool DepositoFresco::podeRemover() const {
    return (this->qntProdutosAtual > 0);
}

bool DepositoFresco::armazenarProdutos(const vector<string> produtos) {
    if (!this->podeAdicionar()) {
        return false;
    }

    typename vector<string>::const_iterator itr;
    for (itr = produtos.begin(); itr != produtos.end(); ++itr) {
        paletes.at(numPaleteAdicionar).push((*itr));
        qntProdutosAtual++;
        numPaleteAdicionar++;

        if (numPaleteAdicionar == this->getNrPaletes()) {
            numPaleteAdicionar = 0;
        }
    }
}

vector<string> DepositoFresco::retirarProdutos(int qntProdutos) {

    vector<string> produtosRetirados;
    if (!this->podeRemover()) {
        return produtosRetirados;
    }

    for (int i = 0; i < qntProdutos; i++) {
        produtosRetirados.push_back(paletes.at(numPaleteRemover).front());
        paletes.at(numPaleteRemover).pop();
        qntProdutosAtual--;
        numPaleteRemover++;

        if (numPaleteRemover == this->getNrPaletes()) {
            numPaleteRemover = 0;
        }

    }
    return produtosRetirados;
}

void DepositoFresco::escrever(ostream& out) const {
    out << "Tipo: Deposito Fresco" << endl;
    Deposito::escrever(out);
    queue<string> temp;
    typename vector<queue<string> >::const_iterator itr;
    for (itr = paletes.begin(); itr != paletes.end(); ++itr) {
        temp = *itr;
        while (!temp.empty()) {
            cout << "Palete: " << temp.front() << endl;
            temp.pop();
        }
    }
    out << endl;
}

ostream& operator<<(ostream& out, DepositoFresco& depF) {
    depF.escrever(out);
    return out;
}

#endif	/* DEPOSITOFRESCO_H */

